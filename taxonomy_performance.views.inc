<?php

/**
 * @file
 * Provides views data for taxonomy.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function taxonomy_performance_views_data_alter(&$data) {
  $data['node_field_data']['term_node_tid_depth1'] = [
    'help' => t('Display content if it has the selected taxonomy terms, or children of the selected terms. Due to additional complexity, this has fewer options than the versions without depth.'),
    'real field' => 'nid',
    'argument' => [
      'title' => t('Performance: Has taxonomy term ID (with depth)'),
      'id' => 'taxonomy_performance_index_tid_depth',
      'accept depth modifier' => TRUE,
    ],
    'filter' => [
      'title' => t('Performance: Has taxonomy terms (with depth)'),
      'id' => 'taxonomy_performance_index_tid_depth',
    ],
  ];

  $data['node_field_data']['term_node_tid_depth_modifier1'] = [
    'title' => t('Performance: Has taxonomy term ID depth modifier'),
    'help' => t('Allows the "depth" for Taxonomy: Term ID (with depth) to be modified via an additional contextual filter value.'),
    'argument' => [
      'id' => 'taxonomy_performance_index_tid_depth_modifier',
    ],
  ];
}
