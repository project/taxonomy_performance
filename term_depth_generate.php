<?php
use Drupal\taxonomy\Entity\Term;
$depth=80;
$vid = 'tags';
$each_depth_has_childs = 2;
$name = 'tag-';
$parent_id = 0;
for ($i=1; $i <= $depth; $i++) {
    if($i == 1){
    	$name = $name . $i;
		$term = Term::create([
		  'name' => $name , 
		  'vid' => $vid ,
		]);
		$term->save();
		if($term){
			$parent_id = $term->id();
		}
    }
    else {
    	$name = $name . '.' . $i;
		$term = Term::create([
		  'name' => $name , 
		  'vid' => $vid ,
		  'parent' => [ $parent_id ],
		]);
		$term->save();
		if($term)
			$parent_id = $term->id();
    } 
}