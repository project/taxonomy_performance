<?php

/**
 * @file
 * Enables the organization of content into categories.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * @defgroup node_taxonomy_index Taxonomy indexing
 * @{
 * Functions to maintain taxonomy indexing.
 *
 * Taxonomy uses default field storage to store canonical relationships
 * between terms and fieldable entities. However its most common use case
 * requires listing all content associated with a term or group of terms
 * sorted by creation date. To avoid slow queries due to joining across
 * multiple node and field tables with various conditions and
 * order by criteria, we maintain a denormalized table with all relationships
 * between terms, published nodes and common sort criteria such
 * as status and created. When using other field storage engines or
 * alternative methods of denormalizing this data you should set the
 * taxonomy.settings:maintain_index_table to '0' to avoid unnecessary writes in
 * SQL.
 */

/**
 * Implements hook_help().
 */
function taxonomy_performance_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.taxonomy_performance':
      $output = '';
      $output .= '<h3>' . t('Commerce Product Taxonomy Filter') . '</h3>';
      $output .= '<p>' . t('The commerce product taxonomy filter module provides filter for categories product') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert() for node entities.
 */
function taxonomy_performance_taxonomy_term_insert(EntityInterface $term) {
  // Add taxonomy index entries for the node.
  taxonomy_performance_build_taxonomy_term_index($term);
}

/**
 * Builds and inserts taxonomy index entries for a given node.
 *
 * The index lists all terms that are related to a given
 * node entity, and is therefore maintained
 * at the entity level.
 *
 * @param \Drupal\node\Entity\Product $node
 *   The node entity.
 */
function taxonomy_performance_build_taxonomy_term_index(Term $term) {
    $depth = [];
    $parent_term_id = $term->parent->target_id;
    while($parent_term_id != 0){
      $depth[] = $parent_term_id;
      $parent_term_id = Term::load($parent_term_id)->parent->target_id;
    }
    $connection = \Drupal::database();
    $table = 'taxonomy_performance_index'; 
    $query = $connection->merge($table)->key([
      'tid' => $term->id(),
    ]);
    if(count($depth)>0){
      $fields=[];  
      $schema = $connection->schema();
      foreach ($depth as $key => $value) {
        $field = 'l'.($key + 1).'_id';
        if($schema->fieldExists($table, $field) == FALSE){
          $spec = [    
            'description' => 'The Level'.($key + 1).' term ID.',
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'default' => 0,
          ];
          $schema->addField($table, $field, $spec);
        }
        $fields[$field] = $value;
      }
      $query->fields($fields); 
    }
    $query->execute();
}

/**
 * Implements hook_ENTITY_TYPE_update() for node entities.
 */
function taxonomy_performance_taxonomy_term_update(Term $term) {
  taxonomy_performance_delete_taxonomy_term_index($term);
  taxonomy_performance_build_taxonomy_term_index($term);
}

/**
 * Implements hook_ENTITY_TYPE_predelete() for node entities.
 */
function taxonomy_performance_taxonomy_term_predelete(Term $term) {
  // Clean up the {node_taxonomy_index} table.
  // When nodes are deleted.
  taxonomy_performance_delete_taxonomy_term_index($node);
}

/**
 * Deletes taxonomy index entries for a given node.
 *
 * @param \Drupal\Core\Entity\EntityInterface $node
 *   The node entity.
 */
function taxonomy_performance_delete_taxonomy_term_index(Term $term) {
    \Drupal::database()->delete('taxonomy_performance_index')->condition('tid', $term->id())->execute();
}
