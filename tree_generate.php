<?php
$em = \Drupal::entityTypeManager();
$vocabularies = $em->getStorage('taxonomy_vocabulary')->loadMultiple();
$depth_data = [];
foreach ($vocabularies as $vocabulary) {       
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vocabulary->id());
    $term_tree = [];
    $terms_data = [];
    foreach ($terms as $key => $term_data) {
        $terms_data[$term_data->tid] = $term_data;
    }
    foreach ($terms_data as $tid => $term_data) {
        $depth_data[$tid] = [];
        while ($term_data->depth != 0) {
            $depth_data[$tid][$term_data->depth] = $term_data->tid;
            $term_data = $terms_data[$term_data->parents[0]];
        }
        $depth_data[$tid][$term_data->depth] = $term_data->tid;
        $depth_data[$tid] = array_values($depth_data[$tid]);
    }
}
print_r($depth_data);