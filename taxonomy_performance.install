<?php

/**
 * @file
 * Install, uninstall, update and schema functions.
 */

use Drupal\commerce_product\Entity\Product;

/**
 * Implements hook_uninstall().
 */
function taxonomy_performance_uninstall() {
  \Drupal::database()->schema()->dropTable('taxonomy_performance_index');
}

/**
 * Implements hook_install().
 */
function taxonomy_performance_install() {
  $em = \Drupal::entityTypeManager();
  $vocabularies = $em->getStorage('taxonomy_vocabulary')->loadMultiple();
  $depth_data = [];
  foreach ($vocabularies as $vocabulary) {       
      $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vocabulary->id());
      $term_tree = [];
      $terms_data = [];
      foreach ($terms as $key => $term_data) {
          $terms_data[$term_data->tid] = $term_data;
      }
      foreach ($terms_data as $tid => $term_data) {
          $depth_data[$tid] = [];
          while ($term_data->depth != 0) {
              $depth_data[$tid][$term_data->depth] = $term_data->tid;
              $term_data = $terms_data[$term_data->parents[0]];
          }
          $depth_data[$tid][$term_data->depth] = $term_data->tid;
          $depth_data[$tid] = array_values($depth_data[$tid]);
      }
  }
  $connection = \Drupal::database();
  foreach ($depth_data as $tid => $depth) {
    $query = $connection->merge('taxonomy_performance_index')->key([
      'tid' => $tid,
    ]);
    unset($depth[0]);
    if(count($depth)>0){
      $fields=[];  
      foreach ($depth as $key => $value) {
        $fields['l'.$key.'_id'] = $value;
      }
      $query->fields($fields); 
    }
    $query->execute();
  }
}

/**
 * Implements hook_schema().
 */
function taxonomy_performance_schema() {
  $schema['taxonomy_performance_index'] = [
    'description' => 'Maintains denormalized information about commerce_product/term relationships.',
    'fields' => [
      'tid' => [
        'description' => 'The term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l1_id' => [
        'description' => 'The Level1 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l2_id' => [
        'description' => 'The Level2 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l3_id' => [
        'description' => 'The Level3 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l4_id' => [
        'description' => 'The Level4 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l5_id' => [
        'description' => 'The Level5 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l6_id' => [
        'description' => 'The Level6 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l7_id' => [
        'description' => 'The Level7 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l8_id' => [
        'description' => 'The Level8 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l9_id' => [
        'description' => 'The Level9 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'l10_id' => [
        'description' => 'The Level10 term ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['tid'],
    'foreign keys' => [
      'term' => [
        'table' => 'taxonomy_term_data',
        'columns' => ['tid' => 'tid'],
      ],
    ],
  ];
  return $schema;
}
