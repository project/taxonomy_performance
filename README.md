# Taxonomy Performance

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Upgrading

INTRODUCTION
------------

This module provides taxonomy query performance. 

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Download module using composer 

 2. Enable the 'taxonomy_performance' module and desired sub-modules in 'Extend'.
   (/admin/modules)

CONFIGURATION
-------------

 * Configure your Taxonomy view with 'Performance: Has taxonomy term ID (with depth)'
 * Go to your Taxonomy Views.
	> Note: Advanced -> Contextual filters -> Add -> 'Performance: Has taxonomy term ID (with depth)'
